<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default theme
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the registered themes you want to use
    | as a default theme.
    |
    | You may activate other theme in runtime code:
    |     Theme::activate($some_theme_name).
    |
    */

    'default' => env('ACTIVE_THEME', ''),

    /*
    |--------------------------------------------------------------------------
    | Auto activation for the default theme
    |--------------------------------------------------------------------------
    |
    | When set to true, the default theme will be activated.
    |
    | This may be usefull, if you do not use the theme middleware or runtime
    | activating.
    |
    */

    'auto_activate' => env('AUTO_ACTIVATE_THEME', ''),

    /*
    |--------------------------------------------------------------------------
    | Application themes path
    |--------------------------------------------------------------------------
    |
    | All your application themes is stored at this location.
    |
    */

    'path' => resource_path('themes'),

    /*
    |--------------------------------------------------------------------------
    | Assets prefix
    |--------------------------------------------------------------------------
    |
    | This option points to a public URL prefix themes assets is fetched from.
    |
    | Each theme will have its own directory inside:
    |     - vendor/themes/blue
    |     - vendor/themes/red
    |     - vendor/themes/winter
    |     - vendor/themes/...
    |
    | Contents of these directories is linked to a source of theme assets
    | using command:
    |     php artisan themes:link
    |
    | In your views you can access these assets using the Blade directive:
    |     <img src="@theme_url('some-image.jpg')">
    |
    | Also you can fetch an asset from a specific theme only:
    |     <img src="@theme_url('some-image.jpg', 'winter')">
    |
    */

    'assets_prefix' => 'vendor/themes',

    /*
    |--------------------------------------------------------------------------
    | Themes list
    |--------------------------------------------------------------------------
    |
    | This array contains registered themes.
    | You may also register themes at runtime:
    |     Theme::add('red');
    |     Theme::add('red', storage_path('some-other-place'));
    |
    */

    'themes' => [

        // Short syntax:
        // 'red',

        // Specified theme source:
        // 'blue' => resource_path('special/blue'),

        // Specified theme source as an array config:
        // 'winter' => [
        //     'source' => resource_path('special/winter'),
        // ]

        // Specified theme source and extended from another theme:
        // 'winter-sale' => [
        //     'source' => resource_path('special/winter-sale'),
        //     'extends' => 'winter',
        // ]

    ],

];
