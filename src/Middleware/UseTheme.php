<?php

namespace Lamotivo\Themes\Middleware;

use Closure;
use Lamotivo\Themes\ThemeFacade as Theme;

class UseTheme
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure                  $next
     * @param  string                    $theme_ame
     * @return mixed
     */
    public function handle($request, Closure $next, $theme_name = null)
    {
        if ( ! $theme_name)
        {
            $theme_name = config('themes.default');
        }

        Theme::activate($theme_name);

        return $next($request);
    }
}