<?php

namespace Lamotivo\Themes;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * @var array
     */
    protected $commands = [
        'Lamotivo\Themes\Commands\LinkCommand',
        'Lamotivo\Themes\Commands\ListCommand',
        'Lamotivo\Themes\Commands\MakeCommand',
    ];

    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/themes.php', 'themes');

        if ($this->app->runningInConsole())
        {
            $this->publishes([
                __DIR__ . '/../config/themes.php' => config_path('themes.php'),
            ], 'config');
        }

        $this->registerBladeDirectives();

        $this->app->make('theme.manager')->addMultiple(config('themes.themes'));

        if (config('themes.auto_activate'))
        {
            $this->app->make('theme.manager')->activateDefault();
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerThemeManager();
        $this->commands($this->commands);
    }


    /**
     * Register the Lamotivo Theme instance.
     *
     * @return void
     */
    protected function registerThemeManager()
    {
        $this->app->singleton('theme.manager', function ($app) {
            $config = $app['config']->get('themes');
            $viewFinder = $app['view']->getFinder();
            $files = $app['files'];

            $themeManager = new ThemeManager($config, $viewFinder, $files);

            return $themeManager;
        });

        $this->app->make('theme.manager')->all();
    }

    /**
     * Register Blade directives.
     *
     * @return void
     */
    protected function registerBladeDirectives()
    {
        Blade::directive('theme_url', function($params) {
            return "<?= app('theme.manager')->url($params) ?>\n";
        });

        Blade::directive('theme_public_path', function($params) {
            return "<?= app('theme.manager')->publicPath($params) ?>\n";
        });

        Blade::directive('theme_svg', function($params) {
            return "<?= app('theme.manager')->svg($params) ?>\n";
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['theme.manager'];
    }
}

