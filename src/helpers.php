<?php

if ( ! function_exists('theme_url'))
{
    function theme_url($url, $theme = null)
    {
        return app()->make('theme.manager')->url($url, $theme);
    }
}

if ( ! function_exists('theme_public_path'))
{
    function theme_public_path($url, $theme = null)
    {
        return app()->make('theme.manager')->publicPath($url, $theme);
    }
}

if ( ! function_exists('theme_svg'))
{
    function theme_svg($url, $theme = null)
    {
        return app()->make('theme.manager')->svg($url, $theme);
    }
}
