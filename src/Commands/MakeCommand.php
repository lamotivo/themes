<?php

namespace Lamotivo\Themes\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

use Lamotivo\Themes\ThemeFacade as Theme;

class MakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:theme {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new theme';

    public function handle()
    {
        $name = $this->argument('name');

        if (Theme::exists($name))
        {
            $this->error('Theme [' . $name . '] already exists.');
            return;
        }

        $path = Theme::path($name);

        if (file_exists($path))
        {
            $this->error('Directory [' . $path . '] already exists.');
            return;
        }

        mkdir($path, 0777, true);

        $theme = Theme::add($name);

        mkdir($theme->getViews(), 0777, true);
        mkdir($theme->getAssets(), 0777, true);
        mkdir($theme->getAssets() . '/js', 0777, true);
        mkdir($theme->getAssets() . '/css', 0777, true);
        mkdir($theme->getPublicAssets(), 0777, true);

        $friendly_name = title_case(Str::snake(camel_case($name), ' '));

        $meta = [
            'name' => $friendly_name,
            'description' => 'The ' . $friendly_name . ' theme for Lamotivo',
            'author' => '',
            'date' => date('Y-m-d'),
        ];

        file_put_contents($path . '/theme.json', json_encode($meta, 480));

        $this->info('Theme [' . $name . '] successfully created!');
        $this->info('Remember register your theme to [config/themes.php] or use Theme::add(\'' . $name . '\') in runtime code.');
    }


}
