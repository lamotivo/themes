<?php

namespace Lamotivo\Themes\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

use Lamotivo\Themes\ThemeFacade as Theme;

class LinkCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'themes:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a symlink from public path to themes assets';


    public function handle()
    {
        $themes = Theme::all();

        foreach ($themes as $theme)
        {
            $symlink = Theme::symlink($theme);

            if ($symlink)
            {
                $this->info('Linked: ' . str_replace(base_path() . DIRECTORY_SEPARATOR, '', $symlink));
            }
        }
    }


}
