<?php

namespace Lamotivo\Themes\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

use Lamotivo\Themes\ThemeFacade as Theme;

class ListCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'themes:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all themes';

    /**
     * The table headers for the command.
     *
     * @var array
     */
    protected $headers = ['Theme', 'Location', 'Parent'];


    public function handle()
    {
        $themes = Theme::all();

        $list = [];

        foreach ($themes as $theme)
        {
            $list[] = [
                $theme->getName(),
                str_replace(base_path() . DIRECTORY_SEPARATOR, '', $theme->getLocation()),
                $theme->getParent(),
            ];
        }

        $this->table($this->headers, $list);

    }


}
