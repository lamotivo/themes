<?php

namespace Lamotivo\Themes;

use InvalidArgumentException;
use Illuminate\Support\HtmlString;
use Illuminate\View\FileViewFinder;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Support\Htmlable;
use Lamotivo\Assets\AssetFacade as Asset;

class Theme
{
    protected $name;
    protected $location;
    protected $parent = null;
    protected $meta = null;
    protected $scss_vars = null;

    public function __construct($name, $location, $parent = null)
    {
        $this->name = $name;
        $this->location = $location;
        $this->parent = $parent;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function getViews()
    {
        return $this->location . '/views';
    }

    public function getAssets()
    {
        return $this->location . '/assets';
    }

    public function getPublicAssets()
    {
        return $this->location . '/assets/public';
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getScssVars()
    {
        if ($this->scss_vars === null)
        {
            if (file_exists($this->location . '/scss_vars.php'))
            {
                $this->scss_vars = include $this->location . '/scss_vars.php';
            }
            else
            {
                $this->scss_vars = [];
            }
        }

        return $this->scss_vars;
    }

    public function extends($parent)
    {
        if (is_a($parent, static::class))
        {
            $this->parent = $parent->getName();
        }
        else
        {
            $this->parent = $parent;
        }
        return $this;
    }

    public function locates($location)
    {
        $this->location = $location;
        return $this;
    }

    public function getMeta()
    {
        if ($this->meta === null)
        {
            $this->loadMeta();
        }

        return $this->meta;
    }

    /**
     * Load theme meta.
     *
     * @return void
     */
    public function loadMeta()
    {
        if (!is_dir($this->location))
        {
            throw new \Exception('Theme [' . $this->name . '] not found.');
        }

        if (file_exists($this->location . '/theme.json'))
        {
            $json = file_get_contents($this->location . '/theme.json');
            $this->meta = collect(json_decode($json));
        }
        else
        {
            $this->meta = collect([]);
        }
    }

}
