<?php

namespace Lamotivo\Themes;

use InvalidArgumentException;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\HtmlString;
use Illuminate\Filesystem\Filesystem;
use Illuminate\View\Factory as ViewFactory;
use Illuminate\View\FileViewFinder;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Support\Htmlable;
use Lamotivo\Assets\AssetFacade as Asset;
use Config;

class ThemeManager
{
    /**
     * Hint path delimiter value.
     *
     * @var string
     */
    const HINT_PATH_DELIMITER = '::';

    /**
     * Laravel's Filesystem.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * Laravel's FileViewFinder.
     *
     * @var FileViewFinder
     */
    protected $viewFinder;

    /**
     * Theme assets paths.
     *
     * @var array
     */
    protected $assets_paths = [];

    /**
     * The array of themes.
     *
     * @var array
     */
    protected $themes = [];

    /**
     * Default themes path.
     *
     * @var string
     */
    protected $path = '';

    /**
     * Default theme name.
     *
     * @var string
     */
    protected $default = '';

    /**
     * Public assets prefix.
     *
     * @var string
     */
    protected $assets_prefix = 'vendor/themes';

    /**
     * Create a new ThemeManager instance.
     *
     * @param  array  $config
     * @return void
     */
    public function __construct($config, FileViewFinder $viewFinder, Filesystem $files)
    {
        $this->files = $files;
        $this->viewFinder = $viewFinder;

        $this->path = Arr::get($config, 'path', resource_path('themes'));
        $this->assets_prefix = Arr::get($config, 'assets_prefix', 'vendor/themes');

        $this->default = Arr::get($config, 'default');
    }

    public function addMultiple($themes)
    {
        if (!empty($themes))
        {
            foreach ($themes as $name => $path)
            {
                if (is_numeric($name))
                {
                    $this->add($path, $this->path . '/' . $path);
                }
                else if (is_array($path))
                {
                    $this->add(
                        $name,
                        Arr::get($path, 'source'),
                        Arr::get($path, 'extends')
                    );
                }
                else
                {
                    $this->add($name, $path);
                }
            }
        }
    }

    /**
     * Add a theme from a custom location.
     *
     * @param string $name       Theme name.
     * @param string $location   Theme location.
     * @param string $extends    Parent theme name.
     *
     * @return ThemeManager
     */
    public function add($name, $location = null, $extends = null)
    {
        if (isset($this->themes[$name]))
        {
            return $this->themes[$name];
        }

        if ( ! $location)
        {
            $location = $this->path . '/' . $name;
        }

        $theme = new Theme($name, rtrim($location, '/'), $extends);

        $this->themes[$name] = $theme;

        return $theme;
    }


    /**
     * Theme path.
     *
     * @param string $path
     *
     * @return bool
     */
    public function path($path = null)
    {
        return $this->path . ($path ? '/' . $path : '');
    }

    /**
     * Check if a theme exists.
     *
     * @return bool
     */
    public function exists($name)
    {
        return isset($this->themes[$name]);
    }

    /**
     * Get all the themes.
     *
     * @return ThemeManager
     */
    public function all()
    {
        return $this->themes;
    }

    /**
     * Get a theme.
     *
     * @param string $name     Theme name.
     *
     * @return ThemeManager
     */
    public function get($name)
    {
        if (isset($this->themes[$name]))
        {
            return $this->themes[$name];
        }

        throw new InvalidArgumentException('Theme [' . $name . '] not found.');
    }

    /**
     * Get a theme.
     *
     * @param Theme $theme     Theme.
     *
     * @return ThemeManager
     */
    public function symlink($theme)
    {
        $source = $theme->getPublicAssets();
        $symlink = null;

        if (is_dir($source))
        {
            if ( ! file_exists(public_path($this->assets_prefix)))
            {
                mkdir(public_path($this->assets_prefix), 0777, true);
            }

            $symlink = public_path($this->assets_prefix . '/' . $theme->getName());

            if ( ! file_exists($symlink))
            {
                $this->files->link($source, $symlink);
            }
        }


        return $symlink;
    }

    /**
     * The array of assets that have been located.
     *
     * @var array
     */
    protected $assets = [];

    /**
     * Flush the cache of located assets.
     *
     * @return void
     */
    public function flush()
    {
        $this->assets = [];
    }

    /**
     * Get an asset url.
     *
     * @param string $asset_file     Asset file.
     * @param string $theme_name     Theme name.
     *
     * @return string
     */
    public function url($asset_file, $theme_name = null)
    {
        if ($this->isNamespaced($asset_file))
        {
            list($theme_name, $asset_file) = $this->parseNamespaceSegments($asset_file);
        }

        if ($theme_name)
        {
            $path = $this->assets_prefix . '/' . $theme_name . '/' . $asset_file;

            return url($path);
        }
        else
        {
            if ( ! isset($this->assets[$asset_file]))
            {
                foreach ($this->assets_paths as $path)
                {
                    $path = $path . '/' . $asset_file;
                    if (file_exists(public_path($path)))
                    {
                        $this->assets[$asset_file] = $path;
                        break;
                    }
                }
            }

            if ( ! empty($this->assets[$asset_file]))
            {
                return url($this->assets[$asset_file]);
            }
        }

        throw new InvalidArgumentException('Asset [' . $asset_file . '] not found.');
    }

    /**
     * Get an asset public path.
     *
     * @param string $asset_file     Asset file.
     * @param string $theme_name     Theme name.
     *
     * @return string
     */
    public function publicPath($asset_file, $theme_name = null)
    {
        if ($this->isNamespaced($asset_file))
        {
            list($theme_name, $asset_file) = $this->parseNamespaceSegments($asset_file);
        }

        if ($theme_name)
        {
            $path = $this->assets_prefix . '/' . $theme_name . '/' . $asset_file;

            return public_path($path);
        }
        else
        {
            if ( ! isset($this->assets[$asset_file]))
            {
                foreach ($this->assets_paths as $path)
                {
                    $path = $path . '/' . $asset_file;
                    if (file_exists(public_path($path)))
                    {
                        $this->assets[$asset_file] = $path;
                        break;
                    }
                }
            }

            if ( ! empty($this->assets[$asset_file]))
            {
                return public_path($this->assets[$asset_file]);
            }
        }

        throw new InvalidArgumentException('Asset [' . $asset_file . '] not found.');
    }

    /**
     * The array of processed SVG files.
     *
     * @var array
     */
    protected $svg_files = [];

    /**
     * Get an SVG file contents.
     *
     * @param string $svg_file       SVG file name.
     * @param string $theme_name     Theme name.
     *
     * @return string
     */
    public function svg($svg_file, $theme_name = null)
    {
        if (!Str::endsWith($svg_file, '.svg'))
        {
            $svg_file .= '.svg';
        }

        if ($this->isNamespaced($svg_file))
        {
            list($theme_name, $svg_file) = $this->parseNamespaceSegments($svg_file);
        }

        if ( ! isset($this->svg_files[$svg_file]))
        {
            $svg_path = $this->publicPath($svg_file, $theme_name);

            $contents = file_get_contents($svg_path);

            if (preg_match('~<svg (.+)</svg>~s', $contents, $match))
            {
                $this->svg_files[$svg_file] = '<svg style="fill:currentColor" ' . $match[1] . '</svg>';
            }
            else
            {
                $this->svg_files[$svg_file] = '';
            }
        }

        return $this->svg_files[$svg_file];
    }


    protected $current = null;

    /**
     * Get current theme.
     *
     * @return Theme
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Activate default theme.
     *
     * @return void
     */
    public function activateDefault()
    {
        if ($this->default && ! $this->activated_themes)
        {
            $this->activate($this->default);
        }
    }

    protected $activated_themes = [];

    /**
     * Activate a theme.
     *
     * @param string $name     Theme name.
     *
     * @return ThemeManager
     */
    public function activate($name = null)
    {
        if ( ! $name)
        {
            return;
        }

        /**
         * Checking if already activated
         */
        if (in_array($name, $this->activated_themes))
        {
            return;
        }


        /**
         * Marking as activated
         */
        $this->activated_themes[] = $name;


        /**
         * Getting the theme to be current
         */
        $current = $this->get($name);


        /**
         * Activating parents
         */
        if ($current->getParent())
        {
            static::activate($current->getParent());
        }


        /**
         * Activating the current theme
         */
        $this->current = $current;


        /**
         * To use theme public assets
         */
        array_unshift($this->assets_paths, $this->assets_prefix . '/' . $this->current->getName());


        /**
         * Getting the views path
         */
        $viewsPath = $this->current->getViews();


        /**
         * Dirty hack to use custom error pages, provided by theme
         */
        Config::prepend('view.paths', $viewsPath);


        /**
         * To use theme views
         */
        $this->viewFinder->prependLocation($viewsPath);


        /**
         * To use theme namespaced views
         */
        $hints = $this->viewFinder->getHints();

        foreach ($hints as $namespace => $hint)
        {
            if (is_dir($path = $viewsPath . '/vendor/' . $namespace))
            {
                $this->viewFinder->prependNamespace($namespace, $path);
            }
        }


        /**
         * To use theme assets
         */
        $assetsPath = $this->current->getAssets();

        Asset::prependLocation($assetsPath);


        /**
         * To use namespaced assets
         */
        $hints = Asset::getHints();

        foreach ($hints as $namespace => $hint)
        {
            if (is_dir($path = $assetsPath . '/vendor/' . $namespace))
            {
                Asset::prependNamespace($namespace, $path);
            }
        }


        /**
         * Adding SCSS vars to the Asset manager
         */
        Asset::addScssVars($this->current->getScssVars());
    }


    /**
     * Returns whether or not the asset name is namespaced.
     *
     * @param  string  $name
     * @return bool
     */
    public function isNamespaced($name)
    {
        return strpos($name, static::HINT_PATH_DELIMITER) > 0;
    }


    /**
     * Get the segments of an asset name with a namespaced name.
     *
     * @param  string  $name
     * @return array
     *
     * @throws \InvalidArgumentException
     */
    protected function parseNamespaceSegments($name)
    {
        $segments = explode(static::HINT_PATH_DELIMITER, $name);
        if (count($segments) !== 2)
        {
            throw new InvalidArgumentException('Asset [' . $name . '] has an invalid name.');
        }
        if (! isset($this->themes[$segments[0]]))
        {
            throw new InvalidArgumentException('Theme [' . $segments[0] . '] not found.');
        }
        return $segments;
    }

}
