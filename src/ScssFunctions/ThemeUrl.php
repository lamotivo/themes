<?php

namespace Lamotivo\Themes\ScssFunctions;

use Lamotivo\Themes\ThemeFacade as Theme;

use Illuminate\Support\Arr;

use ScssPhp\ScssPhp\Value\SassString;

class ThemeUrl
{
    public function __invoke($args)
    {
        $asset_file = null;
        $theme_name = null;
        $default = isset($args[2]) ? $args[2] : '';

        if (isset($args[0]) && $args[0][0] === 'string' && isset($args[0][2]))
        {
            $asset_file = Arr::get($args[0][2], 0);
        }

        if (isset($args[1]) && $args[1][0] === 'string' && isset($args[1][2]))
        {
            $theme_name = Arr::get($args[1][2], 0);
        }

        if (!$asset_file)
        {
            return $this->prepareValue($default);
        }

        try
        {
            $url = Theme::url($asset_file, $theme_name);
            return $this->prepareValue('url("' . $url . '")');
        }
        catch (\Exception $e)
        {
            return $this->prepareValue($default);
        }
    }

    /**
     * Allow serialization for Laravel config cache
     *
     * @return static
     */
    public static function __set_state(array $array = null)
    {
        return new static;
    }

    /**
     * Prepares a value in the return statement
     *
     * @return ScssPhp\ScssPhp\Value\Value|any
     */
    public function prepareValue($value)
    {
        if (class_exists(SassString::class)) {
            return new SassString($value, false);
        }

        return $value;
    }

    public static function getArgumentDeclaration()
    {
        return ['url', 'default:""'];
    }
}
