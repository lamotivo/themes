# Lamotivo Themes

## Introduction

A Laravel package to manage themes in your application.

## Installation

```
composer require lamotivo/themes
```

Then, publish the config

```
php artisan vendor:publish --provider="Lamotivo\Themes\ThemeServiceProvider" --tag=config
```

## Theme structure

Default themes location:

- `resources/themes`

You are free to set your own location in `config/themes.php`.

Directory structure for a theme named **red**:

- `resources/themes/red/views`
- `resources/themes/red/assets/js`
- `resources/themes/red/assets/css`
- `resources/themes/red/assets/public`

Views from themes have an advantage over `resources/views/`.

They also override namespaced views.

## Using theme

You may activate a theme using the following:

- at runtime:

```php
<?php

use Theme;

Theme::activate('red');
```

- using middleware:

```php
<?php

// app\Http\Kernel.php

protected $middlewareGroups = [
    'web' => [
        // ...
        \Lamotivo\Themes\Middleware\UseTheme::class,
    ],
    // ...
];

protected $routeMiddleware = [
    // ...
    'theme' => \Lamotivo\Themes\Middleware\UseTheme::class,
];
```

- at .env file:

```env
ACTIVE_THEME=red
AUTO_ACTIVATE_THEME=true
```

Then you may use it in your routes:

```php
Route::group([
    'prefix' => '/red-theme',
    'middleware' => 'theme:red',
], function() {
    // ...
});
```

## Accessing theme assets

Theme public assets are located at the `public/vendor/themes` directory by default.
You can override the `vendor/themes` prefix in `config/themes.php`.

To access assets you should create symbolic links to your themes.

Use an artisan command to achieve this:

```bash
php artisan themes:link
```

This will create themes links from `public/vendor/themes/*` to `resources/themes/*/assets/public`.

In your Blade views you can use the `@theme_url` helper:

```html
<img src="@theme_url('some-image.jpg')">
```

The example above will produce code like that:

```html
<img src="/vendor/themes/red/some-image.jpg">
```

Sometimes you may want to fetch some asset from a fixed theme like so:

```html
<img src="@theme_url('some-image.jpg', 'winter')">
```

The example above will produce code like that:

```html
<img src="/vendor/themes/winter/some-image.jpg">
```

If the active theme extends from another one, assets are also inherited.

## Handling JS and CSS/SCSS assets

All JS and CSS assets is located by default at `assets/js` and `assets/css` paths relatively to theme location.

You can use any other structure to your liking.

We use [lamotivo/assets](https://packagist.org/packages/lamotivo/assets) to handle assets.
In your Blade views you can use code like this:

```html
@asset_add('js/some-script.js')
@asset_add('js/another-script.js')
@asset_add('css/some-styles.scss')
<!DOCTYPE html>
<html>
<head>
    <title></title>
    @asset_css
</head>
<body>
    <!-- ... -->
    @asset_js
</body>
</html>
```

If the active theme extends from another one, JS and CSS assets are also inherited.
